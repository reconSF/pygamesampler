from flask import render_template
from app import app
from app.models import Game
from app.decorators import debug


@app.route('/')
@app.route('/index')
def index():
    liked_list = Game.query.all()
    return render_template('index.html', liked_games=liked_list)